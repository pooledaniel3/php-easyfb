<?php

require_once "inc/easyfb.php";
$fb = new easyfb([160, 100], "rgb(0,0,128)");

require_once "inc/fbHTML.php";
$screen = new fbHTML("Game landscape", "1mm");

$screen->mode(13);

/*

Pallette:
* 0  - 64: deep blue (0, 0, 128) to light blue (64, 128, 255)
* 64 - 96: deep green (0, 128, 0)
* 96 - 224: deep green (0, 128, 0) to light brown (255, 160, 96)
* 224 - 255: dark gray (128, 128, 128) to white (255, 255, 255)

*/

foreach(range(0, 63) as $i) {
  $screen->palette($i, array($i, $i*2, 128+$i*2), False);
}

foreach(range(64, 96) as $i) {
  $screen->palette($i, array(0, ($i-64)+96, 0), False);
}

foreach(range(96, 223) as $i) {
  $screen->palette($i, array(($i-96)*2, 128+($i-96)/4, ($i-96)/4*3), False);
}

foreach(range(224, 255) as $i) {
  $screen->palette($i, array(128+($i-192)*2, 128+($i-192)*2, 128+($i-192)*2), False);
}

/*
blank zero elevation map (ground level = 64; medium height = 160)
*/

$fb->line([0,0], [160,100], 100, "BF");

/*
pre-scar elevation map with white noise (crater?)
*/

function crater($location = array(10,10), $radius = 10, $height = 10) {
  $fb = new easyfb([160, 100], 0);
    
  $max = $radius;
  foreach(range(0, $max, 0.1) as $r) {
    $depth = pow($r/$max,2)*$height - $height/2;
    $fb->circle($location, $r, $depth);
  }
  
  $max = $radius/10; //rim of crater is 110% size
  foreach(range(0, $max, 0.1) as $r) {
    $depth = pow(1-($r/$max),2)*$height/2;
    $fb->circle($location, $radius + $r, $depth);
  }
  
  return $fb;
}

//crater($fb, [80,50], 30, 100);
foreach(range(0,10) as $i) {
  $size = rand(1,20);
  $fb->overlay(
    crater([rand(0+$size,160-$size),rand(0+$size,100-$size)], $size, $size*rand(5,7))
  );
}

/*
create mountain-ranges and valleys (cut gradients of elevation changes along a line?).
*/

function ridgeSegment($fb, $location, $l, $length, $ridgeHeight) {
  $steep = 10;
  
  if($ridgeHeight > 255) { $ridgeHeight = 255; }

  if(floor($ridgeHeight) > 0) {
    foreach(range(0, $ridgeHeight) as $h) {
      $fb->pset([$location[0]+$l-$length/2, $location[1]+$h-$ridgeHeight/2], ($ridgeHeight/2 - abs($ridgeHeight/2 - $h))*$steep);
    }
  }
}

function ridge($location = array(10,10), $height = 10, $length = 10) {
  $fb = new easyfb([160, 100], 0);
  
  foreach(range(0, $length/4) as $l) {  
    $ridgeHeight = ($l/($length/4))*$height;
    $ridgeHeight += rand(-1, 1);
    ridgeSegment($fb, $location, $l, $length, $ridgeHeight);
  }

  foreach(range(1, $length/2) as $l) {    
    $location[1] += rand(-1, 1);
  
    $ridgeHeight = $height;     
    $ridgeHeight += rand(-1, 1);
    ridgeSegment($fb, $location, $l+(1/4)*$length, $length, $ridgeHeight);
  }

  foreach(range(0, $length/4) as $l) {  
    $ridgeHeight = ($l/($length/4))*$height;
    $ridgeHeight += rand(-1, 1);
    ridgeSegment($fb, $location, $length-$l, $length, $ridgeHeight);
  }
  
  return $fb;
}

/* TODO ridges should be rotatable (use rotatable overlay of arbitrary size?) */
//$fb->overlay(ridge([60,40], 30, 100));
foreach(range(0,10) as $i) {
  $size = rand(10,30);
  if(round($size) != 0) {
    $fb->overlay(
      ridge([rand(0,160),rand(0,100)], $size, abs($size)*2)
    );
  }
}



/*

Droplet falls in random location
droplet looks around for lower elevation -- if one is found moves in that direction
on succesful move, droplet deepens pixel on moved-from location, accumulates speed token
on failed move, droplet looses speed token, deposits pixel on current location
droplet has a random chance to exit (evaporate) on each failed move.

*/





//$fb->pset(array(5,5), "red");

/*
$fb->line([10,10], [150,90], "transparent", "B");

foreach(range(10+1, 150-1) as $x) {
  $fb->line([$x, 10+1], [$x, 90-1], $x-10);
}
*/

/*
$fb->cls();

foreach(range(0, 255, 0.1) as $r) {
  $fb->circle([80,50], $r, abs(255-$r*3));
}
*/

$screen->render($fb->frameBuffer);

