<?php

class easyfb {

  private $size; //array(width, height)
  
  public $validateCoordinates = True;
  
  //the frameBuffer is an array of rows, which are arrays of side-by-side pixels
  public $frameBuffer = array();

  public function __construct($size = array(10, 10), $background = 0) {
    $this->size = $size;
    $this->frameBuffer[-1][-1] = $background; //store background in the invalid location
    $this->fill(array(0,0), $size, $background);
  }
  
  public function cls() {
    $this->fill([0,0], $this->size, $this->frameBuffer[-1][-1]);
  }

  private function fill($start = array(0,0), $end = array(5, 5), $color = 0) {
    for($x = $start[0]; $x <= $end[0]; $x++) {
      for($y = $start[1]; $y <= $end[1]; $y++) {
        $this->frameBuffer[$y][$x] = $color;
      }
    }
  }
  
  private function validateCoordinates($coords = array(0,0)) {
    if($coords[0] < 0 || $coords[1] < 0 
    || $coords[0] > $this->size[0] 
    || $coords[1] > $this->size[1] ) { 
      $coords[0] = -1; 
      $coords[1] = -1; 
    }
    return $coords;
  }
  
  public function pset($coords = array(0,0), $color = 0) {
    if($this->validateCoordinates) { $coords = $this->validateCoordinates($coords); }
    $this->frameBuffer[$coords[1]][$coords[0]] = $color;
  }

  public function point($coords = array(0,0)) {
    if($this->validateCoordinates) { $coords = $this->validateCoordinates($coords); }
    return $this->frameBuffer[$coords[1]][$coords[0]];
  }

  public function line($start = array(0,0), $end = array(INF, INF), $color = 0, $BF = "") {
    if($this->validateCoordinates || in_array(INF, $end)) {
      $start = $this->validateCoordinates($start);
      $end = $this->validateCoordinates($end);
    }
    
    //do not draw lines into invalid locations
    if($start !== [-1,-1] && $end !== [-1,-1]) {
    
      if($BF == "B") {

        $corner[0] = array($start[0], $end[1]);
        $corner[1] = array($end[0], $start[1]);
        
        $this->straightLine($start, $corner[0], $color);
        $this->straightLine($start, $corner[1], $color);
        $this->straightLine($end, $corner[0], $color);
        $this->straightLine($end, $corner[1], $color);
        
      } elseif($BF == "BF") {
      
        $this->fill($start, $end, $color);
      
      } else {
      
        if($start == $end) {
          $this->pset($start, $color); //dodge division by zero in Bresenham's algorithm
        } elseif($start[0] == $end[0] || $start[1] == $end[1]) {
          $this->straightLine($start, $end, $color);
        } else {
          $this->bresenhamLine($start, $end, $color);
        }

      }
  
    } else {
      //ugly workaround for painting off-screen locations
      $this->pset($start, $color);
      $this->pset($end, $color);
    }
      
  }
  
  private function straightLine($start = array(0,0), $end = array(0,5), $color = 0) {
  
    //vertical lines are drawn sideways
    if($start[0] == $end[0]) {
      $X = 0; $Y = 1;
    } else {
      $X = 1; $Y = 0;
    }
        
    $position[$X] = $start[$X];
    foreach(range($start[$Y], $end[$Y]) as $position[$Y]) {
      $this->frameBuffer[$position[1]][$position[0]] = $color;
    }
    
  }
  
  private function bresenhamLine($start = array(0,0), $end = array(5,5), $color = 0) {
 
    //Bresenham's line algorithm -- sortof
    $delta[0] = $end[0] - $start[0];
    $delta[1] = $end[1] - $start[1];

    //this decides which way the line is steep to ensure all pixels are covered
    //lines that have more Y pixels than X pixels are drawn sideways, so swap the coordinates for the math
    if(abs($delta[0]) >= abs($delta[1])) {
      $X = 0; $Y = 1;
    } else {
      $X = 1; $Y = 0; 
    }

    $k = $delta[$Y] / $delta[$X];
    $n = ($k * (-$start[$X])) + $start[$Y];

    foreach(range($start[$X], $end[$X]) as $position[$X]) {
      $position[$Y] = round($k * $position[$X] + $n);
      $this->frameBuffer[$position[1]][$position[0]] = $color;
    }
  
  }
  
  public function circle($center = array(5,5), $radius = 5, $color = 0, $start = 0, $end = INF) {
    if($end == INF) { $end = 2*pi(); }
  
    //https://stackoverflow.com/a/22778207/2897386 -- option B seems more efficient
    $position = $center;
    foreach(range($start, $end, deg2rad(1)) as $angle) {
      $previousPosition = $position;
      $position[0] = round($radius * sin($angle) + $center[0]);
      $position[1] = round($radius * cos($angle) + $center[1]);
      
      if($this->validateCoordinates) { $position = $this->validateCoordinates($position); }
      if($angle != $start) {
        $this->line($previousPosition, $position, $color);
      }

    }
    
  }
  
  //overlay secondary framebuffer, for now this is done using addition only
  public function overlay($fb) {
    for($x = 0; $x <= $this->size[0]; $x++) {
      for($y = 0; $y <= $this->size[1]; $y++) {
        $this->frameBuffer[$y][$x] += $fb->frameBuffer[$y][$x];
      }
    }
  }

}
