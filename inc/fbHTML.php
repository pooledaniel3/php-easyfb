<?php

class fbHTML {

  private $document;
  private $palette;
  private $inverted = false;
  
  function __construct($title = "Frame Buffer", $pixelSize = "10px") {
    require_once "palette.php";
    $this->palette = new palette(); //initialise to mode0: console
  
    require_once "html.php";
    $this->document = new html($title, array(
      "inlinecss" => "td { width: ".$pixelSize."; height: ".$pixelSize."; padding: 0;}"
    ));
  }
  
  public function mode($mode = 0) {
    $this->palette = new palette("mode".$mode);
  }

  public function palette($index, $value, $oldstyle = True) {
    if($oldstyle) {
      $r = ($value % 256) * 4;
      $g = (intdiv($value, 256) % 256) * 4;
      $b = (intdiv($value, 65536)) * 4;
    } else {
      $r = $value[0];
      $g = $value[1];
      $b = $value[2];
    }
    $this->palette->redefine((int) $index,(int) $r,(int) $g,(int) $b);
  }
  
  //this is an incomplete implementation, obviously
  public function window($screen = "") {
    $this->inverted = !(strtolower($screen) == "screen");
  }
  
  public function render($frameBuffer) {
    unset($frameBuffer[-1][-1]); //this is the invalid location, no need to render it
  
    $out = array('<table cellspacing="0">');
    if($this->inverted) { $frameBuffer = array_reverse($frameBuffer); }
    foreach($frameBuffer as $y => $row) {
      $out[] = '<tr>';
      foreach($row as $x => $cell) {
        $out[] = '<td id="'.$x.'x'.$y.'" style="background: '.$this->palette->validateColor($cell).'"></td>';
      }
    }
    $out[] = '</table>';
    echo implode("", $out);
  }
  
}
