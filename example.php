<?php

require_once "inc/easyfb.php";
$fb = new easyfb([160, 100], "black");

require_once "inc/fbHTML.php";
$screen = new fbHTML("Oldskool", "1mm");

$screen->mode(13);

//$fb->pset(array(5,5), "red");

/*
$fb->line([10,10], [150,90], "transparent", "B");

foreach(range(10+1, 150-1) as $x) {
  $fb->line([$x, 10+1], [$x, 90-1], $x-10);
}
*/

foreach(range(0, 100) as $r) {
  $fb->circle([80,50], $r, $r);
}

$screen->render($fb->frameBuffer);

